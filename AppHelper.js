module.exports = {
  API_URL: process.env.NEXT_PUBLIC_BACKEND_URL,
  getAccessToken: () => localStorage.getItem('token'),
  toJSON: (response) => response.json(),
};
