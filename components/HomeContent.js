import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Form, Button, Table } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../AppHelper';
import toNum from '../toNum';

export default function HomeContent() {
  let [description, setDescription] = useState('');
  let [amount, setAmount] = useState('');
  let [transactionType, setTransactionType] = useState('');
  let [selection, setSelection] = useState('');
  let [category, setCategory] = useState();
  let [filled, setFilled] = useState(false);
  let [history, setHistory] = useState();
  let [totalAmount, setTotalAmount] = useState();
  let [totalExpense, setTotalExpense] = useState();
  let [totalIncome, setTotalIncome] = useState();

  // console.log(data);

  useEffect(() => {
    if (
      description !== '' &&
      amount !== '' &&
      transactionType !== '' &&
      selection !== ''
    ) {
      setFilled(true);
    } else {
      setFilled(false);
    }
  }, [description, amount, transactionType, selection]);

  useEffect(() => {
    const getDetails = async () => {
      const options = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      };
      const getCategories = await fetch(
        'https://evening-plateau-28334.herokuapp.com/api/category/transactionHistory',
        options
      );
      const data = await getCategories.json();

      let allData = data.map((indexes) => {
        return indexes;
      });

      let allCategories = allData.filter(
        (match) => match.transactionType === transactionType
      );

      let selectCategory = allCategories.map((categoryIndexes) => {
        return (
          <option key={categoryIndexes._id} value={categoryIndexes.description}>
            {categoryIndexes.description}
          </option>
        );
      });
      setCategory(selectCategory);
    };
    getDetails();
  }, [transactionType]);

  useEffect(() => {
    const transactionHistory = async () => {
      const options = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      };
      const users = await fetch(`${AppHelper.API_URL}/users/details`, options);
      const data = await users.json();
      setTotalAmount(data.totalMoney);

      let array1 = [];
      data.history.map((records) => {
        return array1.push(
          <tr className="records__home" key={records._id}>
            <td className="transactionDetails">{records.transactionType}</td>
            <td className="transactionDetails">{records.description}</td>
            <td className="transactionDetails">$ {records.amount}</td>
          </tr>
        );
      });
      setHistory(array1);
    };

    transactionHistory();
  }, [description, transactionType, amount, totalAmount]);

  useEffect(() => {
    const moneyStatus = async () => {
      const options = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      };

      const getHistory = await fetch(
        `${AppHelper.API_URL}/users/get-history`,
        options
      );
      const data = await getHistory.json();
      // console.log(data);
      let income = 0;
      let expense = 0;
      data.map((eachHistory) => {
        if (eachHistory.transactionType === 'Income') {
          income += toNum(eachHistory.amount);
        } else if (eachHistory.transactionType === 'Expense') {
          expense += toNum(eachHistory.amount);
        }
      });

      setTotalIncome(income);
      setTotalExpense(expense);
    };
    moneyStatus();
  }, [history]);

  const submitTransaction = async (e) => {
    e.preventDefault();
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        description: description,
        categoryName: selection,
        amount: amount,
        transactionType: transactionType,
      }),
    };
    const getTransaction = await fetch(
      `${AppHelper.API_URL}/users/history`,
      options
    );
    const data = await getTransaction.json();
    if (data.success === 'successful') {
      Swal.fire('OK!', 'Successfully added', 'success');
    } else if (data.error === 'Something went wrong') {
      Swal.fire('Oops!', 'Something went wrong', 'error');
    } else if (data.error === 'Duplicate transaction found') {
      Swal.fire('Duplicate transaction found');
    }
    setDescription('');
    setAmount('');
    setTransactionType('');
    setCategory('');
  };

  return (
    <>
      <div className="w-100 transaction__container">
        <div className="history__root">
          <p className="text-center h3 my-2">Transaction Lists</p>
          <div className="history__container">
            <Table className="table__container" hover>
              <tbody>{history}</tbody>
            </Table>
          </div>
        </div>
        <Form
          onSubmit={(e) => submitTransaction(e)}
          className="input__container"
        >
          <p className="text-center mt-3 mb-5 transaction__title">
            Add new Transaction
          </p>
          <p className="text-center mt-0 font-weight-bold">
            Current Money <p className="font-weight-bold">$ {totalAmount}</p>
          </p>
          <div className="d-flex justify-content-center">
            <p className="text-center incomeExpense_text font-weight-bold">
              Total Income
              <p className="font-weight-bold totals__paragraph text-info">
                $ {totalIncome}
              </p>
            </p>
            <p className="text-center incomeExpense_text font-weight-bold">
              Total Expenses
              <p className="font-weight-bold totals__paragraph text-danger">
                $ {totalExpense}
              </p>
            </p>
          </div>
          <Form.Group>
            <Form.Control
              className="p-2"
              value={description}
              placeholder="What is this for?"
              onChange={(e) => setDescription(e.target.value)}
              type="text"
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              className="p-2"
              value={amount}
              placeholder="Amount"
              onChange={(e) => setAmount(e.target.value)}
              type="number"
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              className="p-2"
              as="select"
              value={transactionType}
              onChange={(e) => setTransactionType(e.target.value)}
              required
            >
              <option defaultValue></option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </Form.Control>
          </Form.Group>
          <div className="d-flex w-100 justify-content-between">
            <Form.Group className="w-50">
              <Form.Control
                className="p-2 formCategory"
                as="select"
                onChange={(e) => setSelection(e.target.value)}
                value={selection}
                required
              >
                <option></option>
                {category}
              </Form.Control>
            </Form.Group>
            <Link href="/categories" passHref>
              <Button className="add__categoriesContainer" component="a">
                Add Category
              </Button>
            </Link>
          </div>
          {filled === true ? (
            <Button type="submit" className="p-2 homeButtons" block>
              Submit
            </Button>
          ) : (
            <Button disabled className="p-2 homeButtons" block>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </>
  );
}
