import Head from 'next/head';
import Link from 'next/link';
import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NavBar() {
  const { user } = useContext(UserContext);
  return (
    <>
      <Head>
        <script
          src="https://kit.fontawesome.com/bce12310e4.js"
          crossorigin="anonymous"
        ></script>
      </Head>
      <Navbar
        collapseOnSelect
        expand="lg"
        variant="dark"
        className="NavbarItems"
      >
        <Link href="/">
          <a className="nav__logo">
            <i aria-hidden className="fas fa-coins"></i>Moneygment
          </a>
        </Link>
        <Navbar.Toggle
          className="toggle"
          aria-controls="responsive-navbar-nav"
        />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto">
            <Link href="/">
              <a className="nav__links">Home</a>
            </Link>
            {user.id !== undefined ? (
              <>
                <Link href="/history">
                  <a className="nav__links">History</a>
                </Link>
                <Link href="/dashboard">
                  <a className="nav__links">Dashboard</a>
                </Link>
                <Link href="/setting">
                  <a className="nav__links">Settings</a>
                </Link>
                <Link href="/logout">
                  <a className="nav__links">Logout</a>
                </Link>
              </>
            ) : (
              <>
                <Link href="/login">
                  <a className="nav__links">Login</a>
                </Link>
                <Link href="/register">
                  <a className="nav__links">Register</a>
                </Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}
