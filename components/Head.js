import Head from 'next/head';

export default function head({ data }) {
  const { title } = data;
  return (
    <>
      <Head>
        <title>Moneygment | {title}</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap"
          rel="stylesheet"
        ></link>
        <script
          src="https://kit.fontawesome.com/bce12310e4.js"
          crossorigin="anonymous"
        ></script>
      </Head>
    </>
  );
}
