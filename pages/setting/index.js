import Head from '../../components/Head';
import Link from 'next/link';
import { Button } from 'react-bootstrap';

export default function Settings() {
  const data = { title: 'Settings' };

  return (
    <>
      <Head data={data} />
      <div className="settings__root">
        <p className="text-center settings__title mt-5">
          <i aria-hidden className="fas fa-cog"></i>Settings
        </p>
        <div className="settings__links">
          <Link href="/edit-user-info" passHref>
            <Button className="settingsButton" component="a">
              <i aria-hidden className="fas fa-user-cog"></i> Edit account
              information
            </Button>
          </Link>
          <Link href="/change-password" passHref>
            <Button className="settingsButton" component="a">
              <i aria-hidden className="fas fa-unlock"></i> Change password
            </Button>
          </Link>
        </div>
      </div>
    </>
  );
}
