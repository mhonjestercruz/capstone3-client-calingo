import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Head from '../../components/Head';
import Router from 'next/router';

export default function Categories() {
  const [description, setDescription] = useState('');
  const [type, setType] = useState('');
  const [filled, setFilled] = useState(false);

  const data = { title: 'Categories' };

  useEffect(() => {
    if (description !== '' && type !== '') {
      setFilled(true);
    } else {
      setFilled(false);
    }
  }, [description, type]);

  const category = async (e) => {
    e.preventDefault();
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        description: description,
        transactionType: type,
      }),
    };

    const fetchCategory = await fetch(
      'https://evening-plateau-28334.herokuapp.com/api/category/',
      options
    );
    const data = await fetchCategory.json();
    if (data.success === 'Successfully added') {
      Swal.fire('Yehey!', 'Successfully added', 'success');
    } else if (data.error === 'Something went wrong') {
      Swal.fire('Oops!', 'Something went wrong', 'error');
    } else if (data.error === 'Duplicate found!') {
      Swal.fire('Oops!', 'Duplicate category found', 'error');
    }
    setDescription('');
    setType('');
    Router.push('/');
  };

  return (
    <>
      <Head data={data} />
      <div>
        <Form onSubmit={(e) => category(e)} className="my-5 mx-auto w-50">
          <h1 className="text-center my-5">Add new Category</h1>
          <Form.Group controlId="formBasicDescription">
            <Form.Label>Category Name</Form.Label>
            <Form.Control
              value={description}
              className="p-2"
              type="text"
              placeholder="New Category"
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formBasicSelect">
            <Form.Label>Transaction Type</Form.Label>
            <Form.Control
              className="p-2"
              as="select"
              value={type}
              onChange={(e) => setType(e.target.value)}
            >
              <option defaultValue>Choose your transaction</option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </Form.Control>
          </Form.Group>
          {filled === true ? (
            <Button block className="p-2  homeButtons" type="submit">
              Submit
            </Button>
          ) : (
            <Button block className="p-2  homeButtons" disabled>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </>
  );
}
