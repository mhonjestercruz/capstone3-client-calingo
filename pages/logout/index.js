import Router from 'next/router';
import { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';

export default function index() {
  const { unSetUser, setUser } = useContext(UserContext);

  useEffect(() => {
    //invoke unsetUser to clear local storage
    unSetUser();
    Router.push('/login');
  }, []);

  return null;
}
