import Head from '../components/Head';
import { useContext } from 'react';
import UserContext from '../UserContext';
import HomeContent from '../components/HomeContent';
import Login from './login';

export default function Home() {
  const data = { title: 'Home' };
  const { user } = useContext(UserContext);

  return (
    <>
      <Head data={data} />
      {user.id !== undefined ? <HomeContent /> : <Login />}
    </>
  );
}
