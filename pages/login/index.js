import { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Head from '../../components/Head';
import UserContext from '../../UserContext';
import AppHelper from '../../AppHelper';
import Swal from 'sweetalert2';

export default function index() {
  const { setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  const data = { title: 'Login' };

  useEffect(() => {
    if (email !== '' && password !== '') {
      return setIsActive(true);
    } else {
      return setIsActive(false);
    }
  }, [email, password]);

  const submitForm = async (e) => {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    const loginFetch = await fetch(`${AppHelper.API_URL}/users/login`, options);
    const data = await loginFetch.json();
    if (data.accessToken !== undefined) {
      localStorage.setItem('token', data.accessToken);
      getUserDetails(data.accessToken);
    } else {
      if (data.error === 'No Email found!')
        Swal.fire('Authentication Failed', 'No email found!', 'error');
      else if (data.error === 'Login type error!')
        Swal.fire('Authentication Failed', 'Login type error!', 'error');
      else if (data.error === 'Invalid Password')
        Swal.fire('Authentication Failed', 'Invalid password', 'error');
    }
  };

  const authenticateGoogleToken = async (response) => {
    const options = {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({
        tokenId: response.tokenId,
      }),
    };

    const googleFetch = await fetch(
      `${AppHelper.API_URL}/users/verify-google-id-token`,
      options
    );
    const data = await googleFetch.json();
    if (typeof data !== 'undefined') {
      localStorage.setItem('token', data.accessToken);
      getUserDetails(data.accessToken);
    } else {
      return Swal.fire(
        'Authentication Failed',
        'Something went wrong with the server',
        'error'
      );
    }
  };

  const getUserDetails = async (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    const userDetails = await fetch(
      `${AppHelper.API_URL}/users/details`,
      options
    );

    const data = await userDetails.json();
    if (!data)
      return Swal.fire('Authentication Failed', 'No user data found!', 'error');
    else {
      setUser({ id: data._id });
      Router.push('/');
    }
  };

  return (
    <>
      <Head data={data} />
      <Form onSubmit={(e) => submitForm(e)} className="my-5 mx-auto w-50">
        <Form.Group controlId="email">
          <Form.Label className="font-weight-bold">Email:</Form.Label>
          <Form.Control
            className="forms"
            type="email"
            placeholder="Enter your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label className="font-weight-bold">Password:</Form.Label>
          <Form.Control
            className="forms"
            type="password"
            placeholder="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        {isActive ? (
          <Button
            type="submit"
            className="my-3 homeButtons"
            block
            id="submitButton"
          >
            Submit
          </Button>
        ) : (
          <Button block disabled className="homeButtons my-3">
            Submit
          </Button>
        )}

        <GoogleLogin
          clientId="428162761173-kd2gqvbur9ju7jf95lrf085frao2rb1l.apps.googleusercontent.com"
          buttonText="Login"
          onSuccess={authenticateGoogleToken}
          onFailure={authenticateGoogleToken}
          cookiePolicy={'single_host_origin'}
          className="w-100 text-center d-flex justify-content-center"
        />
      </Form>
    </>
  );
}
