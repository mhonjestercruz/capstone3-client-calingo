import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Head from '../../components/Head';
import AppHelper from '../../AppHelper';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function Forms() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  const data = { title: 'Registration' };

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNumber !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNumber, password1, password2]);

  async function registerUser(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNumber: mobileNumber,
        password: password1,
      }),
    };

    const emailExists = await fetch(
      `${AppHelper.API_URL}/users/email-exist`,
      options
    );

    const emailData = await emailExists.json();

    if (emailData === true)
      return Swal.fire('Duplicate Email!', 'Please use other email', 'error');

    const register = await fetch(`${AppHelper.API_URL}/users`, options);

    const registerData = await register.json();

    if (!registerData)
      return Swal.fire('Oops', 'Something went wrong', 'error');
    else {
      Swal.fire(
        'Successful',
        'You may now login using your new account',
        'success'
      );
    }
    setFirstName('');
    setLastName('');
    setEmail('');
    setMobileNumber('');
    setPassword1('');
    setPassword2('');

    Router.push('/login');
  }
  return (
    <>
      <Head data={data} />
      <div>
        <Form onSubmit={(e) => registerUser(e)} className="my-5 mx-auto w-50">
          <Form.Group controlId="firstName">
            <Form.Label className="font-weight-bold">First Name:</Form.Label>
            <Form.Control
              className="forms"
              type="text"
              placeholder="Enter your first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="lastName">
            <Form.Label className="font-weight-bold">Last Name:</Form.Label>
            <Form.Control
              className="forms"
              type="text"
              placeholder="Enter your last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="email">
            <Form.Label className="font-weight-bold">Email:</Form.Label>
            <Form.Control
              className="forms"
              type="email"
              placeholder="Enter your email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="mobileNumber">
            <Form.Label className="font-weight-bold">Mobile Number:</Form.Label>
            <Form.Control
              className="forms"
              type="number"
              placeholder="Enter your mobile number"
              value={mobileNumber}
              onChange={(e) => setMobileNumber(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label className="font-weight-bold">Password:</Form.Label>
            <Form.Control
              className="forms"
              type="password"
              placeholder="Enter your password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="password2">
            <Form.Label className="font-weight-bold">
              Verify Password:
            </Form.Label>
            <Form.Control
              className="forms"
              type="password"
              placeholder="Enter your password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
          </Form.Group>
          {isActive ? (
            <Button type="submit" block className="homeButtons">
              Submit
            </Button>
          ) : (
            <Button block className="homeButtons" disabled>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </>
  );
}
