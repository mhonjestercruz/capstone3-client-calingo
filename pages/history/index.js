import { useState, useEffect } from 'react';
import { Table, Form } from 'react-bootstrap';
import AppHelper from '../../AppHelper';
import Head from '../../components/Head';

export default function History() {
  const [history, setHistory] = useState('');
  const [filterDescription, setFilterDescription] = useState('');
  const [filterTransactionType, setFilterTransactionType] = useState('');

  const data = { title: 'History' };

  useEffect(() => {
    const transactionHistory = async () => {
      const options = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      };
      const users = await fetch(`${AppHelper.API_URL}/users/details`, options);
      const data = await users.json();

      const myFilteredArray = data.history.filter((match) => {
        return (
          match.description === filterDescription ||
          match.transactionType === filterTransactionType
        );
      });
      let filteredArray = [];
      myFilteredArray.map((items) => {
        let date = new Date(items.createdOn);
        let fixedDate =
          date.getMonth() +
          1 +
          ' / ' +
          date.getDate() +
          ' / ' +
          date.getFullYear();
        return filteredArray.push(
          <tr key={items._id}>
            <td className="p-2 text-center">{fixedDate}</td>
            <td className="p-2 text-center">{items.description}</td>
            <td className="p-2 text-center">{items.transactionType}</td>
            <td className="p-2 text-center">{items.categoryName}</td>
            <td className="p-2 text-center">$ {items.amount}</td>
          </tr>
        );
      });

      let array1 = [];
      data.history.map((filteredItems) => {
        let date = new Date(filteredItems.createdOn);
        let fixedDate =
          date.getMonth() +
          1 +
          ' / ' +
          date.getDate() +
          ' / ' +
          date.getFullYear();
        return array1.push(
          <tr key={filteredItems._id}>
            <td className="p-2 text-center">{fixedDate}</td>
            <td className="p-2 text-center">{filteredItems.description}</td>
            <td className="p-2 text-center">{filteredItems.transactionType}</td>
            <td className="p-2 text-center">{filteredItems.categoryName}</td>
            <td className="p-2 text-center">$ {filteredItems.amount}</td>
          </tr>
        );
      });

      if (filterDescription !== '' || filterTransactionType !== '') {
        setHistory(filteredArray);
      } else {
        setHistory(array1);
      }
    };

    transactionHistory();
  }, [filterDescription, filterTransactionType]);

  return (
    <>
      <Head data={data} />
      <div>
        <Form className="filters__container">
          <Form.Group>
            <Form.Control
              className="form__filterDescription"
              type="text"
              value={filterDescription}
              placeholder="Filter by Description"
              onChange={(e) => setFilterDescription(e.target.value)}
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              className="form__filterTransaction"
              as="select"
              value={filterTransactionType}
              placeholder="Filter by Description"
              onChange={(e) => setFilterTransactionType(e.target.value)}
            >
              <option></option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </Form.Control>
          </Form.Group>
        </Form>
      </div>
      <Table
        striped
        bordered
        hover
        className="transaction__history mx-auto shadow"
      >
        <thead>
          <tr>
            <th className="p-2 text-center">Date</th>
            <th className="p-2 text-center">Description</th>
            <th className="p-2 text-center">Transaction Type</th>
            <th className="p-2 text-center">Category</th>
            <th className="p-2 text-center">Amount</th>
          </tr>
        </thead>
        <tbody>{history}</tbody>
      </Table>
    </>
  );
}
