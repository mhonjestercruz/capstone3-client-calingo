import { useState, useEffect } from 'react';
import Head from '../../components/Head';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../../AppHelper';

export default function ChangePassword() {
  const [oldPassword, setOldPass] = useState('');
  const [newPassword, setNewPass] = useState('');
  const [verifyPass, setVerifyPass] = useState('');
  const [activeButton, setActiveButton] = useState(false);

  const data = { title: 'Change Password' };

  useEffect(() => {
    if (
      oldPassword !== '' &&
      newPassword !== '' &&
      verifyPass !== '' &&
      newPassword === verifyPass
    ) {
      setActiveButton(true);
    } else {
      setActiveButton(false);
    }
  }, [oldPassword, newPassword, verifyPass]);

  const updatingPassword = async (e) => {
    e.preventDefault();
    const options = {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        oldPassword: oldPassword,
        password: newPassword,
      }),
    };
    const fetchPassword = await fetch(
      `${AppHelper.API_URL}/users/change-password`,
      options
    );
    const data = await fetchPassword.json();
    if (data === true) {
      setOldPass('');
      setNewPass('');
      setVerifyPass('');
      return Swal.fire('Ok!', 'Your password is now changed', 'success');
    } else if (data.error === 'Login type error') {
      return Swal.fire('Oops!', 'Login type error', 'error');
    } else if (data.error === 'password does not match') {
      return Swal.fire(
        'Incorrect Old Password',
        'Please put your old password',
        'error'
      );
    }
  };
  return (
    <>
      <Head data={data} />
      <p className="text-center my-5 changepassword__title">
        <i aria-hidden className="fas fa-unlock"></i> Change password
      </p>
      <Form
        className="changePassword__rootContainer"
        onSubmit={(e) => updatingPassword(e)}
      >
        <Form.Group>
          <Form.Control
            className="changePassword__forms"
            value={oldPassword}
            onChange={(e) => setOldPass(e.target.value)}
            type="password"
            placeholder="Old Password"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            className="changePassword__forms"
            value={newPassword}
            onChange={(e) => setNewPass(e.target.value)}
            type="password"
            placeholder="New Password"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            className="changePassword__forms"
            value={verifyPass}
            onChange={(e) => setVerifyPass(e.target.value)}
            type="password"
            placeholder="Verify your Password"
            required
          />
        </Form.Group>
        {activeButton === true ? (
          <Button type="submit" className="add__categoriesContainer" block>
            Submit
          </Button>
        ) : (
          <Button
            disabled
            type="submit"
            className="add__categoriesContainer"
            block
          >
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
