import { Doughnut, Line, Bar } from 'react-chartjs-2';
import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import toNum from '../../toNum';
import Head from '../../components/Head';
import AppHelper from '../../AppHelper';

export default function Dashboard() {
  const [totalMoney, setTotalMoney] = useState();
  const [income, setIncome] = useState();
  const [expense, setExpense] = useState();
  const [totalIncome, setTotalIncome] = useState();
  const [totalExpense, setTotalExpense] = useState();

  const data = { title: 'Dashboard' };

  useEffect(() => {
    const moneyStatus = async () => {
      const options = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      };

      const getHistory = await fetch(
        `${AppHelper.API_URL}/users/details`,
        options
      );
      const data = await getHistory.json();
      let incomeArray = [];
      let expenseArray = [];
      let income = 0;
      let expense = 0;
      data.history.map((eachHistory) => {
        if (eachHistory.transactionType === 'Income') {
          incomeArray.push(eachHistory.amount);
          income += toNum(eachHistory.amount);
        } else if (eachHistory.transactionType === 'Expense') {
          expenseArray.push(eachHistory.amount);
          expense += toNum(eachHistory.amount);
        }
      });
      setTotalMoney(data.totalMoney);
      setIncome(incomeArray);
      setExpense(expenseArray);
      setTotalExpense(expense);
      setTotalIncome(income);
    };
    moneyStatus();
  }, []);

  return (
    <>
      <Head data={data} />
      <div className="dashboard__container">
        <Table className="dashboard__status">
          <thead>
            <tr className="text-center">
              <th>Current Balance</th>
              <th>Total Income</th>
              <th>Total Expenses</th>
            </tr>
          </thead>
          <tbody>
            <tr className="text-center">
              <td>${totalMoney}</td>
              <td>${totalIncome}</td>
              <td>${totalExpense}</td>
            </tr>
          </tbody>
        </Table>
      </div>
      <div className="charts__container">
        <div className="barChart">
          <Bar
            data={{
              datasets: [
                {
                  data: [totalIncome, totalExpense],
                  backgroundColor: ['#4b0082', 'maroon'],

                  label: ['Income'],
                },
              ],
              labels: ['Income', 'Expenses'],
            }}
          />
        </div>

        <div className="doughnutChart">
          <Doughnut
            data={{
              datasets: [
                {
                  data: [totalIncome, totalExpense],
                  backgroundColor: ['#4b0082', 'maroon'],
                },
              ],
              labels: ['Income', 'Expenses'],
            }}
          />
        </div>
      </div>

      <div className="lineChart">
        <Line
          options={{
            responsive: true,
          }}
          data={{
            datasets: [
              {
                backgroundColor: 'rgba(75, 0, 130, 0.65)',
                label: 'Income',
                data: income,
              },
              {
                backgroundColor: 'rgba(128, 0, 0, 0.65)',
                label: 'Expense',
                data: expense,
              },
            ],
            labels: ['1', '2', '3', '4', '5', '6'],
          }}
        />
      </div>
    </>
  );
}
