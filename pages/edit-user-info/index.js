import { Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import Head from '../../components/Head';
import Router from 'next/router';
import AppHelper from '../../AppHelper';

export default function UpdateInfo() {
  const [userFirstName, setUserFirstName] = useState('');
  const [userLastName, setUserLastName] = useState('');
  const [myTotalMoney, setMyTotalMoney] = useState('');
  const [isActive, setIsActive] = useState(false);

  const data = { title: 'User Update' };

  useEffect(() => {
    if (userFirstName !== '' && userLastName !== '' && myTotalMoney !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [userFirstName, userLastName, myTotalMoney]);

  const updateDetails = async (e) => {
    e.preventDefault();

    const options = {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        firstName: userFirstName,
        lastName: userLastName,
        totalMoney: myTotalMoney,
      }),
    };

    const allDetails = await fetch(
      `${AppHelper.API_URL}/users/update-info`,
      options
    );
    const data = allDetails.json();

    if (data) {
      setUserFirstName('');
      setUserLastName('');
      setMyTotalMoney('');
      Swal.fire('Ok!', 'Successfully updated', 'success');
      Router.push('/');
    } else if (data.error === 'No user found')
      return Swal.fire('Oops!', 'Something went wrong', 'error');
  };

  return (
    <>
      <Head data={data} />
      <p className="text-center my-5 changepassword__title">
        <i aria-hidden className="fas fa-users"></i> Update Information
      </p>
      <Form
        className="changePassword__rootContainer"
        onSubmit={(e) => updateDetails(e)}
      >
        <Form.Group>
          <Form.Control
            placeholder="First Name"
            type="text"
            value={userFirstName}
            onChange={(e) => setUserFirstName(e.target.value)}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            placeholder="Last Name"
            type="text"
            value={userLastName}
            onChange={(e) => setUserLastName(e.target.value)}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            placeholder="Update Money"
            type="text"
            value={myTotalMoney}
            onChange={(e) => setMyTotalMoney(e.target.value)}
          />
        </Form.Group>
        {isActive === true ? (
          <Button block type="submit" className="add__categoriesContainer">
            Submit
          </Button>
        ) : (
          <Button
            disabled
            block
            type="submit"
            className="add__categoriesContainer"
          >
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
