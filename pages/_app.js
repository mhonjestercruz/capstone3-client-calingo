import { useState, useEffect } from 'react';
import '../styles/globals.css';
import NavBar from '../components/Navbar';
import Footer from '../components/Footer';
import { UserProvider } from '../UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppHelper from '../AppHelper';

export default function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unSetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
    });
  };

  useEffect(async () => {
    const options = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`,
      },
    };

    const fetchUser = await fetch(
      `${AppHelper.API_URL}/users/details`,
      options
    );
    const data = await fetchUser.json();
    if (typeof data._id !== undefined) {
      setUser({ id: data._id });
    } else {
      setUser({ id: null });
    }
  }, [user.id]);

  return (
    <>
      <div className="position-relative" id="app-div">
        <UserProvider value={{ user, setUser, unSetUser }}>
          <NavBar />
          <Component {...pageProps} />
        </UserProvider>
        <Footer />
      </div>
    </>
  );
}
